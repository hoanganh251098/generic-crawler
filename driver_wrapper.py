from selenium.webdriver.chrome.options import Options
from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
import time
import pdb
import threading

import logging
from selenium.webdriver.remote.remote_connection import LOGGER
LOGGER.setLevel(logging.WARNING)

drivers = []
lock = threading.Lock()

def make_driver(**kwargs):
    option = Options()
    option.add_argument("--no-sandbox")
    option.add_argument("--disable-dev-shm-usage")
    option.add_argument("start-maximized")
    option.add_argument("--disable-infobars")
    option.add_argument("--disable-extensions")
    option.add_argument("--headless")
    if 'headless' in kwargs and kwargs['headless']:
        option.add_argument("--headless")
    # Pass the argument 1 to allow and 2 to block
    option.add_experimental_option("prefs", { 
        "profile.default_content_setting_values.notifications": 1
    })
    driver = webdriver.Chrome(chrome_options=option, executable_path='/usr/lib/chromium-browser/chromedriver')
    driver.set_page_load_timeout(30)

    return driver

# for i in range(4):
#     drivers += [{
#         'status': 'free',
#         'driver': make_driver(headless=True)
#     }]

def get_driver():
    with lock:
        while True:
            driver_id = 0
            for driver in drivers:
                if driver['status'] == 'free':
                    driver['status'] = 'inused'
                    return (driver_id, driver['driver'])
                driver_id += 1
            time.sleep(0.1)

def free_driver(driver_id):
    with lock:
        drivers[driver_id]['status'] = 'free'

class WebElement:
    @classmethod
    def doc(cls, driver):
        return WebElement(driver, driver)

    def __init__(self, e, driver = None):
        self.element = e
        self.driver = driver

    def __repr__(self):
        return f'<{self.element.tag_name} id=\"{self.attr("id")}\" class=\"{self.attr("class")}\">{self.text()}</{self.element.tag_name}>'

    def find(self, selector):
        return [WebElement(e, self.driver) for e in self.element.find_elements_by_css_selector(selector)]

    def findx(self, xpath):
        return [WebElement(e, self.driver) for e in self.element.find_elements_by_xpath(xpath)]

    def attr(self, attribute):
        return self.element.get_attribute(attribute)

    def click(self):
        return self.element.click()

    def val(self):
        return self.element.get_attribute('value')

    def text(self):
        return self.element.text

    def type(self, keys):
        self.element.send_keys(keys)

    def clear(self):
        self.element.clear()

    def save_screenshot(self, path):
        with open(path, 'wb') as file:
            file.write(self.screenshot())

    def screenshot(self):
        return self.element.screenshot_as_png


class Driver:
    def __init__(self, driver = None, **kwargs):
        if not driver:
            self.driver = make_driver(**kwargs)

    def wait_for_page_load(self):
        WebDriverWait(self.driver, 10).until(
            lambda d: d.execute_script('return document.readyState') == 'complete')

    def goto(self, url):
        self.driver.get(url)

    def wait(self, duration):
        time.sleep(duration)

    def get_url(self):
        return self.driver.current_url

    def close(self):
        # self.goto('about:blank')
        # free_driver(self.driver_id)
        self.driver.quit()

    def get_document(self):
        return WebElement.doc(self.driver)

    def get_html(self):
        return self.driver.page_source

    def save_screenshot(self, path):
        self.driver.save_screenshot(path)

    def find_by_text(self, text):
        doc = self.get_document()
        return doc.findx(f'//*[contains(text(), {repr(text)})]')

    def list_related_classes(self, klass):
        doc = self.get_document()
        els = doc.findx(f'//*[contains(@class, {klass})]')
        result = []
        for el in els:
            klass_str = el.attr('class')
            if klass_str == None:
                klass_str = ''
            klasses = filter(lambda e: bool(e), klass_str.split(' '))
            for k in klasses:
                if klass in k and k not in result:
                    result += [k]
        return result

# pdb.set_trace()
