import logging

LOG_FILENAME = './log.txt'
logging.basicConfig(filename=LOG_FILENAME, level=logging.DEBUG)

class BaseSpider:
    def __init__(self, driver, task, output, flags, create_driver):
        self.driver = driver
        self.task = task
        self.output = output
        self.flags = flags
        self.create_driver = create_driver

    def analyze(self):
        return False

    def breakpoint(self):
        if 'cancel' in self.flags and self.flags['cancel'] == True:
            raise Exception('Task cancelled.')
    
    def log(self, message):
        logging.info(message)

    def run(self):
        # do nothing
        pass
