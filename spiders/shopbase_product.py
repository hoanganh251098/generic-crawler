from spiders._base import BaseSpider
from urllib.parse import urlparse, quote, parse_qsl
import requests
import json
import re
import pdb
import time

class Spider(BaseSpider):
    """
        - product
        - thumbnail
        - all images
    """
    def analyze(self):
        domain = urlparse(self.task.input_url).netloc
        path = urlparse(self.task.input_url).path
        collections_url = f'https://{domain}/api/catalog/collections_v2.json'
        res = requests.get(collections_url)
        if res.status_code < 400 and 'application/json' in res.headers.get('content-type') and '/products' in path:
            return True
        return False

    def run(self):
        url = self.task.input_url
        scheme = urlparse(self.task.input_url).scheme
        domain = urlparse(self.task.input_url).netloc
        url_path = urlparse(self.task.input_url).path

        handle = re.search(r'\/products\/[a-zA-Z0-9-_]+', url_path).group().replace('/products/', '')
        
        product_data_url = f'{scheme}://{domain}/api/catalog/product.json?handle={handle}'

        product_data = json.loads(requests.get(product_data_url).text)

        if self.task.task_type == 'thumbnail':
            product_data = {
                'url': url,
                'title': product_data.get('name', ''),
                'images': [product_data.get('thumbnail', '')]
            }
        else:
            product_data = {
                'url': url,
                'title': product_data.get('name', ''),
                'images': [*map(lambda v: v.get('image', ''), product_data['media_gallery'])]
            }
        self.output += [product_data]
