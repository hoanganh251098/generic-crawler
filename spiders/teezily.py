from spiders._base import BaseSpider
from urllib.parse import urlparse
import pdb
import re

class Spider(BaseSpider):
    """
        - list of products
        - thumbnail
        - all images
    """
    def analyze(self):
        self.driver.wait(5)
        doc = self.driver.get_document()
        return len(doc.find('.ais-hits--item')) > 0

    def run(self):
        doc = self.driver.get_document()
        driver2 = self.create_driver()

        product_selector = '.ais-hits--item'
        
        def get_urls():
            return [el.attr('href') for el in doc.find('.ais-hits--item a.campaign-link')]

        def get_original_quality(image_url):
            parsed = urlparse(image_url)
            scheme = parsed.scheme
            domain = parsed.netloc
            path = parsed.path
            new_width = path.split('/')[1] + '0'
            new_height = path.split('/')[2] + '0'
            new_path = (f'/{new_width}/{new_height}/' + '/'.join(path.split('/')[3:])).replace('normal', 'original')
            return scheme + '://' + domain + new_path

        last_urls = []
        while True:
            self.breakpoint()

            for product_el in doc.find(product_selector):
                self.breakpoint()
                url = product_el.find('a.campaign-link')[0].attr('href')
                driver2.goto(url)
                driver2.wait(10)
                doc2 = driver2.get_document()
                title = doc2.find('.campaign-title.ng-binding')[0].text()
                images = [get_original_quality(img_el.attr('src')) for img_el in doc2.find('.campaign-thumbnails img')]

                self.output += [{
                    'url': url,
                    'title': title,
                    'images': images
                }]

                self.task.set_total(len(self.output))

            try:
                try:
                    doc.find('.ais-pagination--link[aria-label="Next"]')[0].click()
                except:
                    self.driver.goto(doc.find('.ais-pagination--link[aria-label="Next"]')[0].attr('href'))
                while get_urls() == last_urls:
                    self.breakpoint()
                    self.driver.wait(2)
                # break
            except:
                break
