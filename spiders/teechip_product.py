from spiders._base import BaseSpider
import pdb
import re
import requests
import lxml.html

class Spider(BaseSpider):
    """
        - product
        - thumbnail
        - all images
    """
    def analyze(self):
        doc = self.driver.get_document()
        return len(doc.find('#contentThumb .ProductTile img.w-full')) > 0

    def run(self):
        doc = self.driver.get_document()
        Driver = type(self.driver)

        title_selector = 'div h1 span' #[0]
        title = doc.find(title_selector)[0].text()
        image_selector = '#contentThumb .ProductTile img.w-full'
        images = [image.attr('src').replace('thumb.jpg', 'regular.jpg') for image in doc.find(image_selector)]

        if self.task.task_type == 'thumbnail':
            images = [images[0]]

        self.output += [{
            'url': self.task.input_url,
            'title': title,
            'images': images
        }]
