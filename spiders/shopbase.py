from spiders._base import BaseSpider
from urllib.parse import urlparse
import requests
import json
import re
import pdb
import time

class Spider(BaseSpider):
    """
        - collection
        - thumbnail
        - all images
    """
    def analyze(self):
        domain = urlparse(self.task.input_url).netloc
        path = urlparse(self.task.input_url).path
        collections_url = f'https://{domain}/api/catalog/collections_v2.json'
        res = requests.get(collections_url)
        self.log(f'domain: {domain} -> status: {res.status_code}')
        self.log(res.text)
        if res.status_code < 400 and 'application/json' in res.headers.get('content-type') and '/search' not in path and '/products' not in path:
            return True
        return False

    def run(self):
        scheme = urlparse(self.task.input_url).scheme
        domain = urlparse(self.task.input_url).netloc
        url_path = urlparse(self.task.input_url).path

        collection_id = None

        collections = []

        # find collection id
        collections_url = f'{scheme}://{domain}/api/catalog/collections_v2.json?page=$page'
        collection_name = re.search(r'/collections/[-\w]+', url_path).group()[len('/collections/'):]
        page = 1
        while True:
            self.breakpoint()
            # self.task.set_msg(f'Checking collections page: {page}')
            collections_data = json.loads(requests.get(collections_url.replace('$page', str(page))).text)['collections']
            if len(collections_data) == 0:
                break
            collections += collections_data
            page += 1

        for collection in collections:
            if collection['url_path'] == collection_name:
                collection_id = collection['id']
        products_url = f'{scheme}://{domain}/api/catalog/products_v2.json?sort_field=created_at&sort_direction=desc&page=$page&collection_ids={collection_id}'

        page = 1
        while True:
            self.breakpoint()
            url = products_url.replace('$page', str(page))
            current_products = json.loads(requests.get(url).text)['products']
            if len(current_products) == 0:
                break
            for product in current_products:
                if self.task.task_type == 'thumbnail':
                    image = ''
                    if 'images' in product and len(product['images']):
                        image = product['images'][0]['src']
                    product_data = {
                        'url': f'{scheme}://{domain}/products/{product["handle"]}',
                        'title': product['title'],
                        'images': [image]
                    }
                    self.output += [product_data]
                else:
                    images = product.get('images', [])
                    product_data = {
                        'url': f'{scheme}://{domain}/products/{product["handle"]}',
                        'title': product['title'],
                        'images': [*map(lambda v: v['src'], images)]
                    }
                    self.output += [product_data]
            page += 1
            self.task.set_total(len(self.output))
            time.sleep(2)
