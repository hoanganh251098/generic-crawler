from spiders._base import BaseSpider
import pdb
import re

class Spider(BaseSpider):
    """
        - product
        - thumbnail
        - all images
    """
    def analyze(self):
        doc = self.driver.get_document()
        try:
            print('woocommerce' in self.driver.driver.page_source)
            return len(doc.find('.thumbnails img')) > 0 and 'woocommerce' in self.driver.driver.page_source
        except:
            return False

    def run(self):
        doc = self.driver.get_document()
        title_selector = '.product_title.entry-title'
        try:
            images = doc.find('.thumbnails img')
            if self.task.task_type == 'thumbnail':
                self.output += [{
                    'url': self.driver.get_url(),
                    'title': doc.find(title_selector)[0].text(),
                    'images': [image[0].attr('src')]
                }]
            else:
                self.output += [{
                    'url': self.driver.get_url(),
                    'title': doc.find(title_selector)[0].text(),
                    'images': [image.attr('src') for image in images]
                }]
        except:
            import traceback
            traceback.print_exc()
