from spiders._base import BaseSpider
import pdb
import re

class Spider(BaseSpider):
    """
        - all products
        - thumbnail
        - all images
    """
    def analyze(self):
        doc = self.driver.get_document()
        return len(doc.find('.product-title > a, .product-loop-wrapper .woocommerce-loop-product__title > a')) > 0 and len(doc.find('.thumbnails img')) == 0

    def run(self):
        doc = self.driver.get_document()
        product_selector = '.product.type-product, .product-loop-wrapper'
        url_selector = '.product-title > a, .product-loop-wrapper .woocommerce-loop-product__title > a'
        title_selector = '.product-title, .product-loop-wrapper .woocommerce-loop-product__title'
        image_selector = 'img, .product-loop-wrapper .woocommerce-loop-product__link img'
        nextbtn_selector = '.next.page-number, .next.page-numbers'
        if self.task.task_type != 'thumbnail':
            driver2 = self.create_driver()

        def get_urls():
            try:
                return [el.attr('href') for el in doc.find(url_selector)]
            except:
                return []

        last_urls = []
        while True:
            self.breakpoint()
            last_urls = get_urls()

            for product_el in doc.find(product_selector):
                self.breakpoint()
                if self.task.task_type == 'thumbnail':
                    url = product_el.find(url_selector)[0].attr('href')
                    title = product_el.find(title_selector)[0].text() or product_el.find(title_selector)[0].find('a')[0].attr('innerHTML')
                    print(product_el.find(title_selector)[0].find('a')[0].attr('innerHTML'))
                    image = product_el.find(image_selector)[0].attr('data-src') or product_el.find(image_selector)[0].attr('src')
                    self.output += [{
                        'url': url,
                        'title': title,
                        'images': [re.sub('-[0-9]+x[0-9]+', '', image)]
                    }]
                else:
                    def get_all_images(url):
                        driver2.goto(url)
                        driver2.wait(2)
                        doc2 = driver2.get_document()
                        images = []
                        for image in doc2.find('.thumbnails img, .wvg-gallery-thumbnail-image img, img[role="presentation"]'):
                            image_url = image.attr('src')
                            if image_url not in images:
                                images += [image_url]
                        if len(images) == 0:
                            for image in doc2.find('.woocommerce-product-gallery__image img'):
                                image_url = image.attr('src')
                                if image_url not in images:
                                    images += [image.attr('src')]
                        return images

                    url = product_el.find(url_selector)[0].attr('href')
                    title = product_el.find(title_selector)[0].text() or product_el.find(title_selector)[0].find('a')[0].attr('innerHTML')
                    self.output += [{
                        'url': url,
                        'title': title,
                        'images': [re.sub('-[0-9]+x[0-9]+', '', image) for image in get_all_images(url)]
                    }]
                self.task.set_total(len(self.output))

            try:
                try:
                    doc.find(nextbtn_selector)[0].click()
                except:
                    self.driver.goto(doc.find(nextbtn_selector)[0].attr('href'))
                while get_urls() == last_urls:
                    self.breakpoint()
                    self.driver.wait(2)
                # break
            except:
                break
