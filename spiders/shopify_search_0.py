from spiders._base import BaseSpider
from urllib.parse import urlparse, urlencode, parse_qsl, urlunparse
import requests
import json
import re
import pdb
import time

class Spider(BaseSpider):
    """
        - search
        - thumbnail
        - all images
    """
    def analyze(self):
        domain = urlparse(self.task.input_url).netloc
        path = urlparse(self.task.input_url).path
        collections_url = f'https://{domain}/collections.json'
        res = requests.get(collections_url)
        if res.status_code < 400 and 'application/json' in res.headers.get('content-type') and '/search' in path:
            return True
        return False

    def run(self):
        doc = self.driver.get_document()
        store_domain = urlparse(self.task.input_url).netloc
        url_selector = 'main a, .product-wrap a'
        nextbtn_selector = '.next a, .cdz-mi-right-arrow'
        last_total = 0

        crawled = []
        while True:
            time.sleep(3)
            self.breakpoint()
            parsed = urlparse(self.driver.get_url())
            page = int(dict(parse_qsl(parsed.query)).get('page', 1))
            q = dict(parse_qsl(parsed.query)).get('q', '')
            urls = doc.find(url_selector)

            for i in range(len(urls)):
                self.breakpoint()
                url = urls[i].attr('href')
                url_scheme = urlparse(url).scheme
                url_domain = urlparse(url).netloc
                url_path = urlparse(url).path
                if url_domain == store_domain and '/products/' in url_path and url not in crawled:
                    product_data = json.loads(requests.get(f'{url_scheme}://{url_domain}{url_path}.json').text)['product']
                    if self.task.task_type == 'thumbnail':
                        try:
                            image = product_data['images'][0]['src']
                        except:
                            image = ''
                        self.output += [{
                            'url': url,
                            'title': product_data.get('title', ''),
                            'images': [image]
                        }]
                        self.task.set_total(len(self.output))
                        crawled += [url]
                        time.sleep(2)
                    else:
                        images = product_data.get('images', [])
                        self.output += [{
                            'url': url,
                            'title': product_data.get('title', ''),
                            'images': [*map(lambda v: v['src'], images)]
                        }]
                        self.task.set_total(len(self.output))
                        crawled += [url]
                        time.sleep(2)

            try:
                if len(crawled) == last_total:
                    break
                parts = list(parsed)
                params = {
                    'page': page + 1,
                    'q': q
                }
                parts[4] = urlencode(params)
                next_url = urlunparse(parts)
                last_total = len(crawled)
                self.driver.goto(next_url)
            except:
                break
