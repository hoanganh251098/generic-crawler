from spiders._base import BaseSpider
from urllib.parse import urlparse
import pdb
import requests
import json
import re

class Spider(BaseSpider):
    """
        - product
        - thumbnail
        - all images
    """
    def analyze(self):
        scheme = urlparse(self.driver.get_url()).scheme
        domain = urlparse(self.driver.get_url()).netloc
        path = urlparse(self.driver.get_url()).path
        url = f'{scheme}://{domain}/_/privacy'
        doc = self.driver.get_document()
        return requests.get(url).status_code < 400 and len(doc.find('.thumb-box img')) > 0 and path != '/' and '/search' not in path

    def run(self):
        doc = self.driver.get_document()

        url = self.task.input_url
        title = doc.find('h3.selected-campaign-mockup-title')[1].text()
        images = doc.find('.thumb-box img')
        images = [image.attr('src').replace('&width=75&height=75', '') for image in images]

        if self.task.task_type == 'thumbnail':
            self.output += [{
                'url': url,
                'title': title,
                'images': [images[0]]
            }]
        else:
            self.output += [{
                'url': url,
                'title': title,
                'images': images
            }]
