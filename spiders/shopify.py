from spiders._base import BaseSpider
from urllib.parse import urlparse
import requests
import json
import re
import pdb
import time

class Spider(BaseSpider):
    """
        - collection
        - thumbnail
        - all images
    """
    def analyze(self):
        domain = urlparse(self.task.input_url).netloc
        path = urlparse(self.task.input_url).path
        collections_url = f'https://{domain}/collections.json'
        res = requests.get(collections_url)
        if res.status_code < 400 and 'application/json' in res.headers.get('content-type') and '/search' not in path and '/products' not in path:
            return True
        return False

    def run(self):
        scheme = urlparse(self.task.input_url).scheme
        domain = urlparse(self.task.input_url).netloc
        url_path = urlparse(self.task.input_url).path

        collection_name = re.search(r'/collections/[-\w]+', url_path).group()[len('/collections/'):]

        products_url = f'{scheme}://{domain}/collections/{collection_name}/products.json?page=$page'

        page = 1
        while True:
            self.breakpoint()
            url = products_url.replace('$page', str(page))
            current_products = json.loads(requests.get(url).text)['products']
            if len(current_products) == 0:
                break
            for product in current_products:
                if self.task.task_type == 'thumbnail':
                    image = ''
                    if 'images' in product and len(product['images']):
                        image = product['images'][0]['src']
                    product_data = {
                        'url': f'{scheme}://{domain}/products/{product["handle"]}',
                        'title': product['title'],
                        'images': [image]
                    }
                    self.output += [product_data]
                else:
                    images = product.get('images', [])
                    product_data = {
                        'url': f'{scheme}://{domain}/products/{product["handle"]}',
                        'title': product['title'],
                        'images': [*map(lambda v: v['src'], images)]
                    }
                    self.output += [product_data]
            page += 1
            self.task.set_total(len(self.output))
            time.sleep(2)
