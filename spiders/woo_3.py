from spiders._base import BaseSpider
import pdb
import re

class Spider(BaseSpider):
    def analyze(self):
        doc = self.driver.get_document()
        return len(doc.find('article .article-inner .entry-title')) > 0 and len(doc.find('.thumbnails img')) == 0

    def run(self):
        doc = self.driver.get_document()
        url_selector = 'article .article-inner .entry-title a'
        title_selector = 'article .article-inner .entry-title'
        image_selector = 'article .article-inner .entry-image img'
        nextbtn_selector = '.next.page-numbers, .next.page-number'

        while True:
            self.breakpoint()
            urls = doc.find(url_selector)
            titles = doc.find(title_selector)
            images = doc.find(image_selector)

            for i in range(len(titles)):
                url = urls[i].attr('href')
                title = titles[i].text()
                image = images[i].attr('src')
                self.output += [{
                    'url': url,
                    'title': title,
                    'images': [re.sub('-[0-9]+x[0-9]+', '', image)]
                }]

            self.task.set_total(len(self.output))

            try:
                doc.find(nextbtn_selector)[0].click()
                self.driver.wait(5)
            except:
                break
