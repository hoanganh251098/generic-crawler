from spiders._base import BaseSpider
from urllib.parse import urlparse
import pdb
import requests
import re
import logging

class Spider(BaseSpider):
    """
        - all products
        - only thumbnail
    """
    def analyze(self):
        scheme = urlparse(self.driver.get_url()).scheme
        domain = urlparse(self.driver.get_url()).netloc
        url = f'{scheme}://{domain}/api/product/v3/products/search?startId=&limit=12'
        return requests.get(url).status_code < 400

    def run(self):
        scheme = urlparse(self.driver.get_url()).scheme
        domain = urlparse(self.driver.get_url()).netloc

        product_selector    = '.ProductItem'
        image_selector      = '.ProductItem img'
        title_selector      = '.ProductItem .Title'
        url_selector        = '.ProductItem .ProductItemInner'

        while True:
            doc = self.driver.get_document()

            image_els = doc.find(image_selector)
            title_els = doc.find(title_selector)
            url_els = doc.find(url_selector)

            for i in range(len(image_els)):
                self.breakpoint()
                image_el = image_els[i]
                title_el = title_els[i]
                url_el = url_els[i]
                image_url = image_el.attr('src')
                self.output += [{
                    'url': url_el.attr('href'),
                    'title': title_el.text(),
                    'images': [image_el.attr('src')]
                }]
            try:
                doc.find('.ButtonPagination')[1].click()
                self.driver.wait(3)
            except Exception as e:
                logging.error(e, exc_info=True)
                break

