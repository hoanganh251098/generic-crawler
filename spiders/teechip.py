from spiders._base import BaseSpider
import pdb
import re
import requests
import lxml.html
import logging

LOG_FILENAME = './log.txt'
logging.basicConfig(filename=LOG_FILENAME, level=logging.DEBUG)


class Spider(BaseSpider):
    """
        - all products
        - thumbnail
        - all images
    """
    def analyze(self):
        doc = self.driver.get_document()
        return len(doc.find('#main-content div.w-full.color-black.truncate')) > 0

    def run(self):
        doc = self.driver.get_document()
        Driver = type(self.driver)

        url_selector = '#main-content a.w-full'
        title_selector = '#main-content div.w-full.color-black.truncate'
        nextbtn_selector = 'a .mdi.mdi-chevron-right'

        def get_urls():
            try:
                return [el.attr('href') for el in doc.find(url_selector)[::2]]
            except:
                return []

        def get_images(url):
            self.log(f'Teechip: Getting images from url: {url}')
            image_selector = '#contentThumb .ProductTile img'
            self.breakpoint()

            html = lxml.html.fromstring(requests.get(url).text)
            self.driver.wait(2)
            images = html.cssselect(image_selector)[::2]
            result = [el.get('src').replace('thumb.jpg', 'regular.jpg') for el in images]

            self.task.set_msg(result[0])
            return result

        last_urls = []
        last_url = ''
        while True:
            self.breakpoint()
            last_urls = get_urls()
            self.task.set_msg(self.driver.get_url())
            self.driver.wait(5)
            urls = doc.find(url_selector)[::2]
            titles = doc.find(title_selector)[::2]

            for i in range(len(titles)):
                self.breakpoint()
                url = urls[i].attr('href')
                title = titles[i].text()
                images = get_images(url)
                if self.task.task_type == 'thumbnail':
                    images = [images[0]]
                self.output += [{
                    'url': url,
                    'title': title,
                    'images': images
                }]
                self.task.set_total(len(self.output))

            last_url = self.driver.get_url()
            try:
                try:
                    doc.find(nextbtn_selector)[0].click()
                except:
                    a_nextbtn_selector = '//a/i[contains(@class, "mdi mdi-chevron-right")]/..'
                    self.driver.goto(doc.findx(a_nextbtn_selector)[0].attr('href'))
                while get_urls() == last_urls or len(get_urls()) == 0 or last_url == self.driver.get_url():
                    self.breakpoint()
                    self.driver.wait(2)
            except Exception as e:
                logging.error(e, exc_info=True)
                self.task.set_msg(str(e))
                break
