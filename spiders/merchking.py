from spiders._base import BaseSpider
from urllib.parse import urlparse
import pdb
import requests
import re

class Spider(BaseSpider):
    """
        - all products
        - thumbnail
        - all images
    """
    def analyze(self):
        scheme = urlparse(self.driver.get_url()).scheme
        domain = urlparse(self.driver.get_url()).netloc
        path = urlparse(self.driver.get_url()).path
        url = f'{scheme}://{domain}/_/privacy'
        doc = self.driver.get_document()
        return requests.get(url).status_code < 400 and len(doc.find('.pager li:nth-last-child(1) a')) == 1 and ('/search' in path or path == '/')

    def run(self):
        driver2 = self.create_driver()
        doc = self.driver.get_document()
        doc2 = driver2.get_document()
        product_selector = '.site-heading-link'
        url_selector = '.site-heading-link'
        title_selector = '.site-heading-link h4'
        image_selector = '.site-heading-link img'
        nextbtn_selector = '.pager li:nth-last-child(1) a'

        while True:
            self.breakpoint()
            urls = doc.find(url_selector)
            titles = doc.find(title_selector)
            images = doc.find(image_selector)

            for i in range(len(titles)):
                self.breakpoint()
                url = urls[i].attr('href')
                title = titles[i].text()
                if self.task.task_type == 'thumbnail':
                    image = images[i].attr('src')
                    self.output += [{
                        'url': url,
                        'title': title,
                        'images': [re.sub('((width|height)=[0-9]+&)+', '', image)]
                    }]
                else:
                    driver2.goto(url)
                    images = doc2.find('.thumb-box img')
                    images = [image.attr('src').replace('&width=75&height=75', '') for image in images]
                    driver2.wait(1)
                    self.output += [{
                        'url': url,
                        'title': title,
                        'images': images
                    }]

                self.task.set_total(len(self.output))

            try:
                if len(doc.find('.pager li.disabled:nth-last-child(1)')) > 0:
                    break
                doc.find(nextbtn_selector)[0].click()
                self.driver.wait_for_page_load()
            except:
                break
