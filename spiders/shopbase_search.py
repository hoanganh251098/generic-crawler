from spiders._base import BaseSpider
from urllib.parse import urlparse, quote, parse_qsl
import requests
import json
import re
import pdb
import time

class Spider(BaseSpider):
    """
        - search
        - thumbnail
        - all images
    """
    def analyze(self):
        domain = urlparse(self.task.input_url).netloc
        path = urlparse(self.task.input_url).path
        collections_url = f'https://{domain}/api/catalog/collections_v2.json'
        res = requests.get(collections_url)
        if res.status_code < 400 and 'application/json' in res.headers.get('content-type') and '/search' in path:
            return True
        return False

    def run(self):
        doc = self.driver.get_document()
        scheme = urlparse(self.task.input_url).scheme
        domain = urlparse(self.task.input_url).netloc
        url_path = urlparse(self.task.input_url).path
        search_string = dict(parse_qsl(urlparse(self.task.input_url).query)).get('q', '')

        products_url = f"{scheme}://{domain}/api/catalog/products_v2.json?q={quote(search_string)}&page=$page"

        page = 1
        while True:
            self.breakpoint()
            url = products_url.replace('$page', str(page))
            current_products = json.loads(requests.get(url).text).get('products', [])
            if len(current_products) == 0:
                break
            for product in current_products:
                if self.task.task_type == 'thumbnail':
                    image = ''
                    if 'images' in product and len(product['images']):
                        image = product['images'][0]['src']
                    product_data = {
                        'url': f'{scheme}://{domain}/products/{product["handle"]}',
                        'title': product['title'],
                        'images': [image]
                    }
                    self.output += [product_data]
                else:
                    images = product.get('images', [])
                    product_data = {
                        'url': f'{scheme}://{domain}/products/{product["handle"]}',
                        'title': product['title'],
                        'images': [*map(lambda v: v['src'], images)]
                    }
                    self.output += [product_data]
            page += 1
            self.task.set_total(len(self.output))
            time.sleep(2)
