from spiders._base import BaseSpider
from urllib.parse import urlparse, urlencode, parse_qsl, urlunparse
import requests
import json
import re
import pdb
import time

class Spider(BaseSpider):
    """
        - product
        - thumbnail
        - all images
    """
    def analyze(self):
        domain = urlparse(self.task.input_url).netloc
        path = urlparse(self.task.input_url).path
        collections_url = f'https://{domain}/collections.json'
        res = requests.get(collections_url)
        if res.status_code < 400 and 'application/json' in res.headers.get('content-type') and '/products' in path:
            return True
        return False

    def run(self):
        self.breakpoint()
        url = self.task.input_url
        parsed = urlparse(url)

        url_scheme = urlparse(url).scheme
        url_domain = urlparse(url).netloc
        url_path = urlparse(url).path

        product_data = json.loads(requests.get(f'{url_scheme}://{url_domain}{url_path}.json').text)['product']
        if self.task.task_type == 'thumbnail':
            try:
                image = product_data['images'][0]['src']
            except:
                image = ''
            self.output += [{
                'url': url,
                'title': product_data.get('title', ''),
                'images': [image]
            }]
        else:
            images = product_data.get('images', [])
            self.output += [{
                'url': url,
                'title': product_data.get('title', ''),
                'images': [*map(lambda v: v['src'], images)]
            }]

