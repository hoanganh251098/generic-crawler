const tasksCache = {};

const watchTask = (task, callback) => {
    const interval = setInterval(() => {
        if(!task) { return; }
        if(['finished', 'error'].includes(task.status)) {
            clearInterval(interval);
        }
        else {
            getTask(task._id, (res) => {
                if(['finished', 'error'].includes(res.status)) {
                    clearInterval(interval);
                }
                if(callback) {
                    callback(res);
                }
            }, (err) => {
                clearInterval(interval);
            });
        }
    }, 5000);
    return interval;
};

const getTask = (id, callback, errorCallback) => {
    $.ajax({
        type: 'get',
        url: `/api/tasks/${id}`,
        dataType: 'json',
        success(res) {
            if(callback) {
                callback(res);
            }
        },
        error(err) {
            if(errorCallback) {
                errorCallback(err);
            }
            console.log('Request error: ', err);
        }
    });
};

const delTask = (id, callback) => {
    $.ajax({
        type: 'delete',
        url: `/api/tasks/${id}`,
        dataType: 'json',
        success(res) {
            callback(res);
        },
        error(err) {
            Swal.fire({
                icon: 'error',
                title: 'Error',
                text: err.responseJSON.msg,
                confirmButtonColor: '#26a69a'
            })
            console.log('Request error: ', err);
        }
    });
};

const cancelTask = (id, callback) => {
    $.ajax({
        type: 'put',
        url: `/api/tasks/${id}`,
        dataType: 'json',
        success(res) {
            if(callback) {
                callback(res);
            }
        },
        error(err) {
            console.log('Request error: ', err);
        }
    });
};

const listTasks = (page, searchString, callback, preloadCallback) => {
    $.ajax({
        type: 'get',
        url: `/api/tasks`,
        dataType: 'json',
        data: {
            page,
            search_string: searchString
        },
        success(res) {
            if(callback) {
                callback(res);
            }
        },
        error(err) {
            console.log('Request error: ', err);
        }
    });
    if(preloadCallback) {
        $.ajax({
            type: 'get',
            url: `/api/tasks`,
            dataType: 'json',
            data: {
                page: page+1,
                search_string: searchString
            },
            success(res) {
                preloadCallback(res);
            },
            error(err) {
                console.log('Request error: ', err);
            }
        });
    }
};

const crawlUrl = (url, task_type, callback) => {
    $.ajax({
        type: 'post',
        url: '/api/tasks',
        data: JSON.stringify({url, task_type}),
        contentType: "application/json; charset=utf-8",
        dataType: 'json',
        success(res) {
            if(callback) {
                callback(res);
            }
        },
        error(err) {
            console.log('Request error: ', err);
        }
    });
};

$(document).ready(() => {
    M.AutoInit();
});
