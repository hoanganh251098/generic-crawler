

$(document).ready(() => {
    $('.only-thumbnail-option').on('click', () => {
        const textAreaEl = $('textarea.url-input');
        const text = textAreaEl.val();
        const urls = text.split('\n').map(v => v.trim()).filter(url => url);
        for(const url of urls) {
            crawlUrl(url, 'thumbnail');
        }
        textAreaEl.val('');
        if(urls.length) {
            Swal.fire({
                title: 'Added to queue!',
                icon: 'success',
                confirmButtonText: 'OK',
                confirmButtonColor: '#26a69a'
            });
        }
    });
    $('.all-images-option').on('click', () => {
        const textAreaEl = $('textarea.url-input');
        const text = textAreaEl.val();
        const urls = text.split('\n').map(v => v.trim()).filter(url => url);
        for(const url of urls) {
            crawlUrl(url, 'all-images');
        }
        textAreaEl.val('');
        if(urls.length) {
            Swal.fire({
                title: 'Added to queue!',
                icon: 'success',
                confirmButtonText: 'OK',
                confirmButtonColor: '#26a69a'
            });
        }
    })
});