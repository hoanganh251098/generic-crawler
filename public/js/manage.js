(() => {
    let watchers = {};
    let displayedTasks = {};

    const renderTask = (task) => {
        const htmlTemplate = `
        <li>
            <div class="collapsible-header"><i class="material-icons downloading">autorenew</i>
                <span class="title"></span>
            </div>
            <div class="collapsible-body">
                <div class="progress">
                    <div class="indeterminate"></div>
                </div>
                <div class="collection">
                    <a class="collection-item task-id">
                        <span class="badge"></span>
                        ID
                    </a>
                    <a class="collection-item status">
                        <span class="badge"></span>
                        Status
                    </a>
                    <a class="collection-item start-url">
                        <span class="badge"></span>
                        Input URL
                    </a>
                    <a class="collection-item total">
                        <span class="badge"></span>
                        Total
                    </a>
                    <a class="collection-item created-date">
                        <span class="badge"></span>
                        Date created
                    </a>
                    <a class="collection-item spider-name">
                        <span class="badge"></span>
                        Spider
                    </a>
                    <a class="collection-item task-type">
                        <span class="badge"></span>
                        Task type
                    </a>
                    <a class="collection-item message">
                        <span class="badge"></span>
                        Message
                    </a>
                </div>
                <a class="waves-effect waves-light btn download-csv-btn disabled">Download CSV</a>
                <a href="" target="_blank" class="waves-effect waves-light btn preview-btn disabled">Preview</a>
                <div class="right">
                    <a class="waves-effect waves-light btn retry-btn disabled">Retry</a>
                    <a class="waves-effect waves-light btn red lighten-1 cancel-task-btn">Cancel</a>
                    <a class="waves-effect waves-light btn red lighten-1 remove-task-btn">Remove</a>
                </div>
            </div>
        </li>
        `;
        let el;
        if(displayedTasks[task._id] === undefined) {
            el = $($.parseHTML(htmlTemplate));
            displayedTasks[task._id] = el;
            $('.collapsible').append(el);
            el.find('.remove-task-btn').on('click', () => {
                Swal.fire({
                    title: 'Are you sure?',
                    icon: 'warning',
                    confirmButtonText: 'Yes',
                    showCancelButton: true,
                    cancelButtonText: 'Cancel',
                    confirmButtonColor: '#26a69a',
                    cancelButtonColor: '#ef5350'
                }).then((result) => {
                    if (result.isConfirmed) {
                        clearInterval(watchers[task._id]);
                        delTask(task._id, () => {
                            displayedTasks[task._id] = undefined;
                            el.remove();
                            if($('.collapsible').children().length === 0) {
                                $('.collapsible').hide();
                            } else {
                                $('.collapsible').show();
                            }
                            reloadLastPage();
                        });
                    } else if (result.isDenied) {
                    }
                });
                
            });
            el.find('.retry-btn').on('click', () => {
                Swal.fire({
                    title: 'Are you sure?',
                    icon: 'warning',
                    confirmButtonText: 'Yes',
                    showCancelButton: true,
                    cancelButtonText: 'Cancel',
                    confirmButtonColor: '#26a69a',
                    cancelButtonColor: '#ef5350'
                }).then((result) => {
                    if (result.isConfirmed) {
                        crawlUrl(task.input_url, task.task_type, () => {
                            clearDisplayedTasks();
                            loadAndRenderTasks();
                        });
                    }
                });
            });
            el.find('.cancel-task-btn').on('click', () => {
                Swal.fire({
                    title: 'Are you sure?',
                    icon: 'warning',
                    confirmButtonText: 'Yes',
                    showCancelButton: true,
                    cancelButtonText: 'Cancel',
                    confirmButtonColor: '#26a69a',
                    cancelButtonColor: '#ef5350'
                }).then((result) => {
                    if (result.isConfirmed) {
                        cancelTask(task._id, () => {
                            el.find('.cancel-task-btn').toggleClass('disabled', true);
                        });
                    } else if (result.isDenied) {
                    }
                });
                
            });
        } else {
            el = displayedTasks[task._id];
        }
        el.find('.collapsible-header .title').text(`Task #${task._id} - ${task.input_url} - ${task.task_type}`);
        el.find('.task-id span').text(task._id);
        el.find('.status span').text(task.status);
        el.find('.start-url span').text(task.input_url);
        el.find('.start-url').attr('href', task.input_url);
        el.find('.start-url').attr('target', '_blank');
        el.find('.total span').text(task.total);
        el.find('.created-date span').text(task.date_created);
        el.find('.spider-name span').text(task.spider_name);
        el.find('.task-type span').text(task.task_type);
        el.find('.message span').text(task.message);
        el.find('.preview-btn').attr('href', `/tasks/${task._id}/products`);
        const v = (new Date).getTime();
        el.find('.download-csv-btn').attr('href', `/api/tasks/${task._id}/download/products.csv?v=${v}`);
        el.find('.preview-btn').toggleClass('disabled', false);
        if(['finished', 'error'].includes(task.status)) {
            el.find('.progress').hide();
            el.find('.collapsible-header i').toggleClass('downloading', false);
            el.find('.retry-btn').toggleClass('disabled', false);
            el.find('.cancel-task-btn').hide();
            el.find('.remove-task-btn').show();
            el.find('.download-csv-btn').toggleClass('disabled', false);
            if(task.status === 'finished') {
                el.find('.collapsible-header i').text('done');
            }
            else if(task.status === 'error') {
                el.find('.collapsible-header i').text('error_outline');
            }
        } else {
            el.find('.progress').show();
            el.find('.cancel-task-btn').show();
            el.find('.remove-task-btn').hide();

            if(task.status === 'pending') {
                el.find('.collapsible-header i').toggleClass('downloading', false);
                el.find('.collapsible-header i').text('pending');
            }
            else {
                el.find('.collapsible-header i').toggleClass('downloading', true);
                el.find('.collapsible-header i').text('autorenew');
            }
        }
        $('.collapsible').collapsible();
        if(watchers[task._id] == undefined) {
            watchers[task._id] = watchTask(task, (newTask) => {
                renderTask(newTask);
            });
        }
    };

    const taskFilter = {
        searchString: '',
        page: 1,
        fullLoaded: false
    };

    const clearDisplayedTasks = () => {
        $('.collapsible').empty();
        taskFilter.page = 1;
        taskFilter.fullLoaded = false;
        displayedTasks = {};
        for(const taskID in watchers) {
            const intervalID = watchers[taskID];
            clearInterval(intervalID);
        }
        watchers = {};
        checkEmptyTasks();
        $('.load-more-btn').show();
    };

    const checkEmptyTasks = () => {
        if($('.collapsible').children().length === 0) {
            $('.collapsible').hide();
            return true;
        } else {
            $('.collapsible').show();
            return false;
        }
    };

    const loadAndRenderTasks = () => {
        if(!taskFilter.fullLoaded) {
            listTasks(taskFilter.page, taskFilter.searchString, (res) => {
                for(const task of res) {
                    renderTask(task);
                }
                checkEmptyTasks();
                taskFilter.page += 1;
            },
            (res) => {
                if(res.length === 0) {
                    taskFilter.fullLoaded = true;
                    $('.load-more-btn').hide();
                }
            });
        }
    };

    const reloadLastPage = () => {
        if(!taskFilter.fullLoaded) {
            listTasks(taskFilter.page - 1, taskFilter.searchString, (res) => {
                for(const task of res) {
                    renderTask(task);
                }
                checkEmptyTasks();
            },
            (res) => {
                if(res.length === 0) {
                    taskFilter.fullLoaded = true;
                    $('.load-more-btn').hide();
                }
            });
        }
    };

    $(document).ready(() => {
        $('#search').on('change', () => {
            clearDisplayedTasks();
            taskFilter.searchString = $('#search').val();
            loadAndRenderTasks();
        });

        $('#search').on('keyup', function (e) {
            if (e.key === 'Enter' || e.keyCode === 13) {
                $('#search').change();
            }
        });

        $('.load-more-btn').on('click', function(e) {
            loadAndRenderTasks();
        });

        clearDisplayedTasks();
        loadAndRenderTasks();

    });

})();
