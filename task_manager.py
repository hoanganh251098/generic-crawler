from model.task import task_table
from product_manager import ProductManager
import time
import re
import os

running_tasks = []

class Task:
    def __init__(self, task_data=None):
        self.task_id = None
        self.status = '' # running | pending | paused | finished | cancelled | analyzing | error
        """
        pending - analyzing - running / paused - finished
                            - running / paused - cancelled
                            - error
        """
        self.input_url = ''
        self.total = 0
        self.created_date = ''
        self.spider = ''
        self.msg = ''
        self.task_type = ''
        if task_data:
            self.load(task_data)

    def serialize(self):
        return {
            "_id": self.task_id,
            "status": self.status,
            "input_url": self.input_url,
            "total": self.total,
            "date_created": self.created_date,
            "spider_name": self.spider,
            "message": self.msg,
            "task_type": self.task_type,
        }

    def __save(self):
        task_table[self.task_id] = self.serialize()

    def load(self, data):
        self.task_id = data['_id']
        self.status = data['status']
        self.input_url = data['input_url']
        self.total = data['total']
        self.created_date = data['date_created']
        self.spider = data['spider_name']
        self.msg = data['message']
        self.task_type = data['task_type']

    def change_status(self, new_status):
        if new_status not in [
            'loading',
            'pending', 'analyzing', 'running', 'generating csv'
            'paused', 'cancelled', 'finished', 'error']:
            assert(f'Invalid status: {new_status}')
        else:
            self.status = new_status
            self.__save()

    def set_msg(self, msg):
        self.msg = msg
        self.__save()

    def set_total(self, total):
        self.total = total
        self.__save()

    def set_spider(self, spider):
        self.spider = spider
        self.__save()

class TaskManager:
    def create_task(self, url, task_type):
        task = Task()
        task_data = {
            "status": "pending",
            "input_url": url,
            "total": 0,
            "date_created": time.strftime("%Y-%m-%d %H:%M"),
            "spider_name": "",
            "message": '',
            "task_type": task_type
        }
        task_id = task_table.insert(task_data)
        task.load(task_table[task_id])
        return task

    def list_tasks(self, **query):
        try:
            page_number = query.get('page_number', 1)
            page_size = query.get('page_size', 10)
            search_string = query.get('search_string', '')
            sql = f'''
            select * from task
                order by _id desc
                limit {(page_number - 1) * page_size}, {page_size};
            '''
            if search_string != '':
                ss = '"%' + search_string.lower().replace('%', r'\%').replace('_', r'\_') + '%"'
                sql = f'''
                select * from task
                    where (lower(spider_name) like {ss} ESCAPE '\\' or 
                        lower(status) like {ss} ESCAPE '\\' or 
                        lower(date_created) like {ss} ESCAPE '\\' or 
                        lower(message) like {ss} ESCAPE '\\' or 
                        lower(task_type) like {ss} ESCAPE '\\' or 
                        lower(input_url) like {ss} ESCAPE '\\')
                    order by _id desc
                    limit {(page_number - 1) * page_size}, {page_size};
                '''
            tasks_data = task_table.query(sql)
            tasks = [Task(task_data) for task_data in tasks_data]
            return tasks
        except:
            import traceback
            traceback.print_exc()
            return []

    def get_task(self, id):
        try:
            task_data = task_table[id]
            return Task(task_data)
        except:
            return None

    def delete_task(self, task):
        if task.status in ['finished', 'error']:
            task_table.remove(task.task_id)
            ProductManager(task.task_id).delete_all_products()
            if os.path.exists(f'./output/{task.task_id}.csv'):
                os.remove(f'./output/{task.task_id}.csv')
            return True
        return False

    def stop_corrupted_tasks(self):
        for task in self.list_tasks(page_number=1, page_size=1000):
            if task.status not in ['finished', 'error']:
                task.change_status('error')
                task.set_msg('Task corrupted')

if __name__ == '__main__':
    def main():
        tm = TaskManager()
        tm.list_tasks(page_number=1)
        print(tm.get_task(0).serialize())
    main()
