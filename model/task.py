from model import Schema, ForeignKey
import json

class Task(Schema):
    status = 'varchar(255)'
    input_url = 'varchar(255)'
    total = 'integer'
    date_created = 'varchar(255)'
    spider_name = 'varchar(255)'
    task_type = 'varchar(255)'
    message = 'text'
    # products

task_table = Task('database.db')
