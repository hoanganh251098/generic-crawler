import sqlite3
import json

memory_db = sqlite3.connect(':memory:')
def make_conn(db_location):
    if db_location == ':memory:':
        return memory_db
    return sqlite3.connect(db_location)

class ForeignKey:
    def __init__(self, name):
        self.name = name

databases = {}
class Schema:
    def __get_proplist(self):
        proplist = dir(self)
        def proplist_filter(v):
            return not v.startswith('_') and v not in ['insert', 'query', 'list', 'fill', 'remove']
        return [*filter(proplist_filter, proplist)]

    def __init__(self, db_location):
        self.__db_location = db_location
        self.__foreign_key_cache = {}
        if db_location not in databases:
            databases[db_location] = {}
        databases[db_location][type(self).__name__] = self
        proplist = self.__get_proplist()
        sql = f'create table if not exists `{type(self).__name__}` (\n'
        sql += '    _id integer primary key,\n'
        foreign_key_setup = ''
        for propname in proplist:
            proptype = self.__getattribute__(propname)
            if type(proptype) == str:
                sql += f'    {propname} {proptype},\n'
            elif type(proptype) == ForeignKey:
                sql += f'    {propname} integer,\n'
                foreign_key_setup += f'    foreign key({propname}) references {proptype.name}(_id),\n'
                self.__foreign_key_cache[proptype.name] = propname
        sql += foreign_key_setup
        sql = sql[:-2] + '\n'
        sql += ');'
        db = make_conn(self.__db_location)
        db.execute(sql)

    def insert(self, data):
        namelist = [*zip(*data.items())][0]
        valuelist = [*map(lambda v: repr(v), [*zip(*data.items())][1])]
        sql = f'insert into `{type(self).__name__}`({", ".join(namelist)}) values({", ".join(valuelist)});'
        db = make_conn(self.__db_location)
        cursor = db.execute(sql)
        db.commit()
        return cursor.lastrowid

    def __make_json(self, cursor):
        namelist = [*map(lambda v: v[0], cursor.description)]
        return [dict(zip(namelist, row), _tablename=type(self).__name__) for row in cursor]

    def list(self, offset, size):
        sql = f'select * from {type(self).__name__} limit {offset}, {size};'
        db = make_conn(self.__db_location)
        cursor = db.execute(sql)
        return self.__make_json(cursor)

    def __getitem__(self, _id):
        sql = f'select * from {type(self).__name__} where _id = {_id}'
        db = make_conn(self.__db_location)
        cursor = db.execute(sql)
        try:
            return self.__make_json(cursor)[0]
        except:
            return None

    def __setitem__(self, _id, data):
        data = data.copy()
        if '_tablename' in data:
            del data['_tablename']
        if '_id' in data:
            del data['_id']
        namelist = [k for k, v in data.items()]
        valuelist = [repr(v) for k, v in data.items()]
        sql = f'''
        replace into {type(self).__name__} (_id, {', '.join(namelist)}) 
            values({_id}, {', '.join(valuelist)});
        '''
        db = make_conn(self.__db_location)
        db.execute(sql)
        db.commit()
    
    def remove(self, _id):
        sql = f'delete from {type(self).__name__} where _id = {_id};'
        db = make_conn(self.__db_location)
        db.execute(sql)
        db.commit()

    def query(self, custom_query):
        db = make_conn(self.__db_location)
        cursor = db.execute(custom_query)
        db.commit()
        try:
            return self.__make_json(cursor)
        except:
            return None

    def fill(self, parent, name):
        db = make_conn(self.__db_location)
        foreign_key_name = self.__foreign_key_cache[parent['_tablename']]
        sql = f'select * from {type(self).__name__} where {foreign_key_name}={parent["_id"]}'
        parent[name] = self.__make_json(db.execute(sql))

def test():
    pass
