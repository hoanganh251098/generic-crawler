from model import Schema, ForeignKey
import json

class Product(Schema):
    url = 'varchar(255)'
    title = 'text'
    images = 'text'
    task_id = ForeignKey('Task')

product_table = Product('database.db')
