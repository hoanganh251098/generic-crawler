from flask import Flask, request, abort, send_from_directory, jsonify, make_response, render_template
from product_manager import ProductManager
from flask_compress import Compress
import concurrent.futures
import jwt
import time
import task_manager
import crawler
import os
import traceback

compress = Compress()

def main():
    app = Flask(__name__)
    app.jinja_env.auto_reload = True
    app.config['TEMPLATES_AUTO_RELOAD'] = True
    compress.init_app(app)
    jwt_secret = "Gy9j6ufPLi"
    tm = task_manager.TaskManager()
    tm.stop_corrupted_tasks()
    executor = concurrent.futures.ThreadPoolExecutor(max_workers=2)
    sessions = {}

    @app.route('/')
    def homepage():
        return render_template('index.html', script_version=time.time_ns())

    @app.route('/manage')
    def manage_page():
        return render_template('manage.html', script_version=time.time_ns())

    @app.route('/advanced')
    def advanced_page():
        spiders = {}
        for spider_name, spider in sorted(crawler.load_spiders().items()):
            spider_doc = spider.Spider.__doc__
            if not spider_doc:
                spider_doc = ''
            spiders[spider_name] = [line.strip() for line in spider_doc.split('\n')]
        return render_template('advanced.html', script_version=time.time_ns(), spiders=spiders)

    @app.route('/tasks/<int:task_id>/products/')
    def tasks_id_products(task_id):
        products = []
        prev_page = 'javascript:void(0)'
        next_page = 'javascript:void(0)'
        try:
            pm = ProductManager(str(task_id))
            page_number = int(request.args.get('page', 1))
            products = pm.list_products(page_number, 20)
            if len(pm.list_products(page_number - 1, 20)) > 0:
                prev_page = f'/tasks/{task_id}/products/?page={page_number-1}'
            if len(pm.list_products(page_number + 1, 20)) > 0:
                next_page = f'/tasks/{task_id}/products/?page={page_number+1}'
        except:
            traceback.print_exc()
        return render_template('products.html', script_version=time.time_ns(), products=products,
            prev_page=prev_page, next_page=next_page)

    @app.route('/js/<path:path>')
    def serve_js(path):
        return send_from_directory('./public/js', path)

    @app.route('/css/<path:path>')
    def serve_css(path):
        print(path)
        return send_from_directory('./public/css', path)

    @app.route('/api/get_token')
    def api_login():
        encoded_jwt = jwt.encode({
            "sessionid": "0000000000",
            "time": str(time.time_ns())
        }, jwt_secret, algorithm="HS256")

        return jsonify({
            'access_token': encoded_jwt
        })

    @app.route('/api/tasks', methods=['GET', 'POST'])
    def api_tasks():
        """
        GET: list tasks
        POST: create task
        """
        if request.method == 'GET':
            page_number = int(request.args.get('page', 1))
            search_string = request.args.get('search_string', '')
            return jsonify([task.serialize() for task in tm.list_tasks(page_number=page_number, search_string=search_string)])
        elif request.method == 'POST':
            url = request.json['url']
            task_type = request.json.get('task_type', 'thumbnail')
            task = tm.create_task(url, task_type)
            future = executor.submit(crawler.crawl, task)
            return jsonify(task.serialize())

    @app.route('/api/tasks/<int:task_id>', methods=['GET', 'PUT', 'DELETE'])
    def api_tasks_id(task_id):
        """
        GET: task info
        PUT: pause/resume task
        DELETE: delete task
        """
        task = tm.get_task(str(task_id))
        if task:
            if request.method == 'GET':
                return jsonify(task.serialize())
            elif request.method == 'PUT':
                if crawler.cancel(task):
                    return jsonify({
                        'msg': 'OK'
                        })
                else:
                    return make_response(jsonify({
                        'status': '409',
                        'msg': 'Cannot cancel task.'
                        }), 409)
            elif request.method == 'DELETE':
                if tm.delete_task(task):
                    return jsonify({
                        'msg': 'OK'
                        })
                else:
                    return make_response(jsonify({
                        'status': '409',
                        'msg': 'Cannot remove task.'
                        }), 409)
        else:
            return make_response(jsonify({
                'status': '404',
                'msg': 'Task not found.'
                }), 404)

    @app.route('/api/tasks/<int:task_id>/products', methods=['GET'])
    def api_tasks_id_products(task_id):
        try:
            pm = ProductManager(str(task_id))
            page_number = int(request.args.get('page', 1))
            return jsonify(pm.list_products(page_number, 20))
        except:
            return make_response(jsonify({
                    'status': '404',
                    'msg': 'Product not found.'
                    }), 404)

    @app.route('/api/tasks/<int:task_id>/products/<int:product_id>', methods=['GET', 'DELETE'])
    def api_tasks_id_products_id(task_id, product_id):
        try:
            pm = ProductManager(str(task_id))
            return jsonify(pm.get_product(product_id))
        except:
            return make_response(jsonify({
                    'status': '404',
                    'msg': 'Product not found.'
                    }), 404)

    @app.route('/api/tasks/<int:task_id>/download/products.csv', methods=['GET'])
    def api_tasks_id_download_csv(task_id):
        task = tm.get_task(str(task_id))
        if task:
            return send_from_directory('./output', f'{task.task_id}.csv')
        abort(404)

    app.run(host='0.0.0.0', port='8000', debug=False)


if __name__ == '__main__':
    main()
