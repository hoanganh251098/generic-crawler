from driver_wrapper import Driver, WebElement
from product_manager import ProductManager
from urllib.parse import urlparse
from flask import Flask, request
import importlib
import traceback
import csv
import pdb
import os

task_map = {}

def load_spiders():
    spiders = {}
    spider_names = sorted(os.listdir('./spiders'))
    for spider_name in spider_names:
        spider_name = os.path.splitext(spider_name)[0]
        spider = importlib.import_module(f'spiders.{spider_name}')
        if 'Spider' in dir(spider):
            del spider.Spider
        importlib.reload(spider)
        if 'Spider' in dir(spider):
            spiders[spider_name] = spider
    return spiders

load_spiders()

def crawl(task):
    output = []
    flags = {}
    task_map[task.task_id] = flags
    pm = ProductManager(task.task_id)
    additional_drivers = []

    def create_driver():
        nonlocal additional_drivers
        new_driver = Driver(headless=True)
        additional_drivers += [new_driver]
        return new_driver

    try:
        driver = Driver(headless=True)
        spiders = load_spiders()

        task.change_status('loading')
        driver.goto(task.input_url)

        task.change_status('analyzing')
        selected_spider = None

        # select spider
        for spider_name, spider in spiders.items():
            try:
                spider_instance = spider.Spider(driver=driver, task=task, output=output, flags=flags, create_driver=create_driver)
                if spider_instance.analyze():
                    selected_spider = spider_instance
                    task.set_spider(spider_name)
                    break
            except:
                traceback.print_exc()

        if selected_spider == None:
            raise Exception(f'Failed to select spider.')

        task.change_status('running')
        selected_spider.run()
        task.change_status('finished')

    except Exception as e:
        task.change_status('error')
        task.set_msg(str(e))
        traceback.print_exc()
        logging.error(e, exc_info=True)

    try:
        filename = f'./output/{task.task_id}.csv'
        with open(filename, 'w', newline='') as csv_file:
            writer = csv.writer(csv_file, delimiter=',', quoting=csv.QUOTE_MINIMAL)
            for product in output:
                writer.writerow((product['url'], product['title'], *product['images']))

    except Exception as e:
        task.change_status('error')
        task.set_msg('Failed to create csv: ' + str(e))
        traceback.print_exc()
        logging.error(e, exc_info=True)

    try:
        driver.close()
        for additional_driver in additional_drivers:
            additional_driver.close()
        for product in output:
            pm.add_product(product)
        task.set_total(len(output))
    except Exception as e:
        traceback.print_exc()
        logging.error(e, exc_info=True)

def cancel(task):
    try:
        flags = task_map[task.task_id]
        flags['cancel'] = True
        return True
    except:
        return False
