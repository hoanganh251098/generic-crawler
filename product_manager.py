import csv
from model.product import product_table
import json

class ProductManager:
    def __init__(self, task_id):
        self.task_id = task_id

    def add_product(self, data):
        data = dict(
            url = data['url'],
            title = data['title'],
            images = json.dumps(data['images']),
            task_id = self.task_id
        )
        product_table.insert(data=data)

    def get_all(self):
        products = product_table.query(f'select * from product where task_id={self.task_id}')
        for product in products:
            product['images'] = json.loads(product['images'])

    def get_product(self, id):
        return product_table[id]

    def list_products(self, page, size):
        offset = (page - 1) * size
        if offset < 0:
            return []
        products = product_table.query(f'select * from product where task_id={self.task_id} limit {offset}, {size}')
        for product in products:
            product['images'] = json.loads(product['images'])
        return products

    def delete_all_products(self):
        product_table.query(f'delete from product where task_id={self.task_id}')
